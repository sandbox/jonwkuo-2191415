<?php
/**
 * The settings form to set categories
 *
 */
function uc_quote_consolidate_settings_form($form, &$form_state){
  $form['add_category'] = array(
    '#type' => 'textfield',
    '#title' => t('Add Shipping Category'),
  );
  $form['add_category_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#submit' => array('uc_quote_consolidate_add_category'),
  );
  $form['categories'] = array(
    '#tree' => TRUE,
  );
  $categories = uc_quote_consolidate_get_categories(TRUE);
  $enabled_categories = array(0 => 'No category');
  if($categories){
    foreach($categories as $key => $category){
      if($category->enabled){
        $enabled_categories[$key] = $category->title;
      }
      $form['categories'][$key]['enabled'] = array(
        '#type' => 'checkbox',
        '#default_value' => $category->enabled,
      );
      $form['categories'][$key]['title'] = array(
        '#type' => 'textfield',
        '#default_value' => $category->title,
      );
      $form['categories'][$key]['weight'] = array(
        '#type' => 'weight',
        '#default_value' => $category->weight,
        '#attributes' => array('class' => array('uc-quote-consolidate-weight')),
      );
      $form['categories'][$key]['delete'] = array(
        '#markup' => l('delete', 'admin/store/settings/quotes/settings/consolidate/' . $key . '/delete'),
      );
    }
  }
  $form['methods'] = array(
    '#tree' => TRUE,
  );
  $methods = uc_quote_methods();
  foreach($methods as $method){
    //Setup a fieldset for each method we can quote for
    if(isset($method['quote'])){
      $form['methods'][$method['id']] = array(
        '#type' => 'fieldset',
        '#title' => $method['title'],
        '#description' => t("Shipping services will be compared and the service with the lowest rate will be used for it's category. You can offset the rate when comparing it to different services to alter the results, but the actual rate will be preserved."),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      //List each enabled service to categorize
      if(isset($method['quote']['accessorials'])){
        $enabled_services = array_filter(variable_get('uc_' . $method['id'] . '_services', $method['quote']['accessorials']));
        foreach($enabled_services as $key => $enabled){
          $method_category = uc_quote_consolidate_get_category_method($method['id'], $key);
          $form['methods'][$method['id']][$key] = array(
            '#type' => 'fieldset',
            '#title' => $method['quote']['accessorials'][$key],
            '#collapsible' => FALSE,
            '#collapsed' => FALSE,
          );
          $form['methods'][$method['id']][$key]['category'] = array(
            '#type' => 'select',
            '#options' => $enabled_categories,
            '#default_value' => ($method_category && !is_null($method_category->cid)) ? $method_category->cid : 0,
            '#title' => t('Category'),
          );
          $form['methods'][$method['id']][$key]['offset'] = array(
            '#type' => 'select',
            '#options' => array('none' => t('None'), 'percentage' => t('Percentage'), 'fixed' => t('Fixed Amount')),
            '#default_value' => $method_category ? $method_category->offset : 'none',
            '#title' => t('Offset Method'),
          );
          $form['methods'][$method['id']][$key]['offset_value'] = array(
            '#type' => 'textfield',
            '#description' => t('A negative value will lower the rate when compared. A positive value will increase the rate when compared.'),
            '#default_value' => $method_category ? $method_category->offset_value : '0.00',
            '#size' => 5,
          );
        }
      }else{
        $method_category = uc_quote_consolidate_get_category_method($method['id']);
        $form['methods'][$method['id']]['default']['category'] = array(
          '#type' => 'select',
          '#options' => $enabled_categories,
          '#default_value' => ($method_category && !is_null($method_category->cid)) ? $method_category->cid : 0,
          '#title' => $method['title'],
        );
        $form['methods'][$method['id']]['default']['offset'] = array(
          '#type' => 'select',
          '#options' => array('none' => t('None'), 'percentage' => t('Percentage'), 'fixed' => t('Fixed Amount')),
          '#default_value' => $method_category ? $method_category->offset : 'none',
          '#title' => t('Offset Method'),
        );
        $form['methods'][$method['id']]['default']['offset_value'] = array(
          '#type' => 'textfield',
          '#description' => t('A negative value will lower the rate when compared. A positive value will increase the rate when compared.'),
          '#default_value' => $method_category ? $method_category->offset_value : '0.00',
          '#size' => 5,
        );
      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
  );

  return $form;
}

/**
 * Creation function to add a shipping category
 *
 */
function uc_quote_consolidate_add_category($form, &$form_state){
  if(!empty($form_state['values']['add_category'])){
    db_insert('uc_shipping_categories')
    ->fields(array('title' => filter_xss_admin($form_state['values']['add_category']), 'weight' => 0, 'enabled' => 1))
    ->execute();
  }else{
    form_set_error('add_category', t('Provide a name to create a category with.'));
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Deletion page to remove a shipping category
 *
 */
function uc_quote_consolidate_delete_category($form, &$form_state, $cid){
  $form['cid'] = array(
    '#type' => 'value',
    '#value' => $cid,
  );

  return confirm_form($form, t('Are you sure you want to delete this shipping category?'), 'admin/store/settings/quotes/settings/consolidate', t('Deleting this category will leave any shipping methods that belonged to this category to be uncategorized.'));
}

/**
 * Deletion confirmation submit function
 *
 */
function uc_quote_consolidate_delete_category_submit($form, &$form_state){
  $cid = $form_state['values']['cid'];
  db_delete('uc_shipping_categories')
  ->condition('cid', $cid)
  ->execute();
  db_update('uc_shipping_methods')
  ->condition('cid', $cid)
  ->fields(array('cid' => NULL))
  ->execute();

  drupal_set_message(t('Shipping category deleted.'));

  $form_state['redirect'] = 'admin/store/settings/quotes/settings/consolidate';
}

/**
 * Validation function for saving method categorization
 * 
 */
function uc_quote_consolidate_settings_form_validate($form, &$form_state) {
  foreach($form_state['values']['methods'] as $id => $values){
    if(isset($values['default'])){
      if(!is_numeric($values['default']['offset_value'])){
        form_set_error('methods][' . $id . '][default][offset_value', t('The offset value must be numeric.'));
      }
    }else{
      foreach($values as $key => $service_values){
        if(!is_numeric($service_values['offset_value'])){
          form_set_error('methods][' . $id . '][' . $key . '][offset_value', t('The offset value must be numeric.'));
        }
      }
    }
  }
}

/**
 * Submit function for saving method categorization
 *
 */
function uc_quote_consolidate_settings_form_submit($form, &$form_state){
  foreach($form_state['values']['categories'] as $cid => $values){
    db_update('uc_shipping_categories')
    ->condition('cid', $cid)
    ->fields(array('title' => filter_xss_admin($values['title']), 'enabled' => $values['enabled'], 'weight' => $values['weight']))
    ->execute();
  }
  foreach($form_state['values']['methods'] as $id => $values){
    if(isset($values['default'])){
      if($values['default']['category'] == 0){
        $values['default']['category'] = NULL;
      }
      db_merge('uc_shipping_methods')
      ->key(array('mid' => $id))
      ->fields(array('mid' => $id, 'aid' => NULL, 'cid' => $values['default']['category'], 'offset' => $values['default']['offset'], 'offset_value' => $values['default']['offset_value']))
      ->execute();
    }else{
      foreach($values as $key => $service_values){
        if($service_values['category'] == 0){
          $service_values['category'] = NULL;
        }
        db_merge('uc_shipping_methods')
        ->key(array('mid' => $id, 'aid' => $key))
        ->fields(array('mid' => $id, 'aid' => $key, 'cid' => $service_values['category'], 'offset' => $service_values['offset'], 'offset_value' => $service_values['offset_value']))
        ->execute();
      }
    }
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Theme function for the shipping category form
 *
 */
function theme_uc_quote_consolidate_settings_form($variables){
  $form = $variables['form'];
  $output = drupal_render($form['add_category']);
  $output .= drupal_render($form['add_category_submit']);

  drupal_add_tabledrag('uc-quote-consolidate-categories', 'order', 'sibling', 'uc-quote-consolidate-weight');
  $header = array(t('Enable'), t('Title'), array('data' => t('List Position'), 'sort' => 'asc'), t('Remove'));
  $rows = array();
  foreach(element_children($form['categories']) as $category){
    $row = array(
      drupal_render($form['categories'][$category]['enabled']),
      drupal_render($form['categories'][$category]['title']),
      drupal_render($form['categories'][$category]['weight']),
      drupal_render($form['categories'][$category]['delete']),
    );
    $rows[] = array(
      'data' => $row,
      'class' => array('draggable'),
    );
  }
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'uc-quote-consolidate-categories'),
    'empty' => t('No shipping categories have been created yet.'),
  ));

  $output .= drupal_render_children($form);
  return $output;
}

