QUOTE CONSOLIDATION README
Quote Consolidation is a module for Ubercart that categorizes shipping methods and their services. It's designed to simplify quoting options by comparing rates within each category and offers the least expensive rate for each category. Administrators can offset rates for comparison to alter the results.

FEATURES
Create and customize as many shipping categories as you want to display to the customer.
Compare similar shipping methods and services and display the cheapest option.
Customer only knows what category they've chosen. Shipping methods are preserved for fulfillment.
